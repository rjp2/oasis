# README #

This is the code used in the paper 
"oASIS: Adaptive Column Sampling for Kernel Matrix Approximation" by R. Patel, T. Goldstein, E.L. Dyer, et al., Department of Electrical and Computer Engineering, Rice University. 

Specifically, this code implements:
oASIS in MATLAB, 
oASIS in MATLAB with mex, and
oASIS-P in C++. 

We also include a MATLAB function to generate BORG datasets.

A link to the paper can be found on arXiv here: 
http://arxiv.org/abs/1505.05208

Contact email: rjp2@rice.edu
http://dsp.rice.edu